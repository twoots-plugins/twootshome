## TwootsHome

Simple Home Plugin.

### Usage

**Basic Commands**

- `/sethome [name]`
    - Set a home
    - name parameter is optional, default is "default"
- `/home [name]`
    - Teleports you to your home
    - name parameter is optional, default is "default"
- `/delhome [name]`
    - Deletes your home
    - name parameter is optional, default is "default"
- `/homes`
    - Lists all of your homes

**Admin Commands**

- `/ahomes <player>`
    - Lists all the homes of a player
- `/ahome <player> [name]`
    - Teleports you to the home of a player
    - name parameter is optional, default is "default"
    
### Permissions

- `twootshome.basic`
    - Gives you access to all basic commands
- `twootshome.admin`
    - Gives you access to all admin commands
    - Defaults to op only