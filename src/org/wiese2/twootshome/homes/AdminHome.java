package org.wiese2.twootshome.homes;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.wiese2.helper.Console;
import org.wiese2.twootshome.Main;
import org.wiese2.twootshome.data.Home;
import org.wiese2.twootshome.data.HomeManager;

public class AdminHome {
    public static boolean doHomeCommands(HomeManager homeManager, CommandSender sender, Player player, Command command, String label, String[] args) {
        Player target;
        if (args.length < 1 || (target = Bukkit.getPlayerExact(args[0])) == null) {
            Console.sendError(sender, "You need to specify a player!");
            return true;
        }

        String name = "default";
        if (args.length > 1) {
            name = args[1];
        }
        String user = target.getUniqueId().toString();
        Location loc = target.getLocation();

        Home home = new Home(loc, name, user);

        if (command.getName().equalsIgnoreCase("ahomes")) {
            String homeList = String.join(", ", homeManager.getHomes(user));

            if (homeList.equals("")) {
                Console.sendError(sender, target.getName() + " doesn't have any homes");
            } else {
                Console.sendNotice(sender, target.getName() + "'s Homes: " + homeList);
            }
            return true;
        } else if (command.getName().equalsIgnoreCase("ahome")) {
            home = homeManager.getHome(home);

            if (home == null) {
                Console.sendError(sender, target.getName() + " doesn't have a home named "+name+"'");
            } else {
                player.teleport(home.getLocation());
                Console.sendSuccess(sender, "You were teleported to " + target.getName() + "'s home '"+name+"'");
                Main.doTeleportSound(player);
            }
            return true;
        }

        return false;
    }
}
