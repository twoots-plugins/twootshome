package org.wiese2.twootshome.homes;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.hover.content.Text;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.wiese2.helper.Console;
import org.wiese2.twootshome.Main;
import org.wiese2.twootshome.data.Home;
import org.wiese2.twootshome.data.HomeManager;

public class PlayerHome {
    public static boolean doHomeCommands(HomeManager homeManager, CommandSender sender, Player player, Command command, String label, String[] args) {
        boolean isConfirm = false;
        if (args.length > 1 && args[1].equalsIgnoreCase("confirm")) {
            isConfirm = true;
        }

        String name = "default";
        if (args.length > 0) {
            name = args[0];
        }
        String user = player.getUniqueId().toString();
        Location loc = player.getLocation();

        Home home = new Home(loc, name, user);

        if (command.getName().equalsIgnoreCase("sethome")) {
            if (homeManager.getHome(home) != null && !isConfirm) {
                sendConfirmMessage("Home '" + name + "' already exists, are you sure you want to overwrite it? Type | to overwrite your home.", "/sethome " + name + " confirm", "Confirm /sethome", player);
                return true;
            }

            home.prepareLocation();
            homeManager.setHome(home);
            Console.sendSuccess(sender, "Your home '" + name + "' was set");
            return true;
        } else if (command.getName().equalsIgnoreCase("home")) {
            home = homeManager.getHome(home);

            if (home == null) {
                Console.sendError(sender, "Home '" + name + "' does not exist");
            } else {
                player.teleport(home.getLocation());
                Console.sendSuccess(sender, "You were teleported to your home '" + name + "'");
                Main.doTeleportSound(player);
            }
            return true;
        } else if (command.getName().equalsIgnoreCase("homes")) {
            String[] homeList = homeManager.getHomes(user);

            if (homeList.length == 0) {
                Console.sendError(sender, "You don't have any homes");
            } else {
                TextComponent message = new TextComponent("Homes: ");
                message.setColor(net.md_5.bungee.api.ChatColor.GRAY);

                boolean first = true;
                for (String homeName : homeList) {
                    if (first) {
                        first = false;
                    } else {
                        TextComponent comma = new TextComponent(", ");
                        comma.setColor(net.md_5.bungee.api.ChatColor.GRAY);
                        message.addExtra(comma);
                    }

                    TextComponent click = new TextComponent(homeName);
                    click.setColor(net.md_5.bungee.api.ChatColor.GRAY);
                    click.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/home " + homeName));
                    click.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("Teleport to " + homeName)));
                    message.addExtra(click);
                }

                player.spigot().sendMessage(message);
            }
            return true;
        } else if (command.getName().equalsIgnoreCase("delhome")) {
            home = homeManager.getHome(home);

            if (home == null) {
                Console.sendError(sender, "Home '" + name + "' does not exist");
            } else {
                if (!isConfirm) {
                    sendConfirmMessage("Are you sure you want to delete your home? Type | to delete it.", "/delhome " + name + " confirm", "Confirm /delhome", player);
                    return true;
                }

                homeManager.removeHome(home);
                Console.sendSuccess(sender, "Home '" + name + "' was deleted");
            }
            return true;
        }
        return false;
    }

    private static void sendConfirmMessage(String messageText, String confirmCommand, String hover, Player player) {
        String[] parts = messageText.split("\\|");

        TextComponent message = new TextComponent(parts[0]);
        message.setColor(net.md_5.bungee.api.ChatColor.AQUA);

        TextComponent click = new TextComponent(confirmCommand);
        click.setColor(net.md_5.bungee.api.ChatColor.RED);
        click.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, confirmCommand));
        click.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(hover)));
        message.addExtra(click);

        TextComponent message2 = new TextComponent(parts[1]);
        message2.setColor(net.md_5.bungee.api.ChatColor.AQUA);
        message.addExtra(message2);

        player.spigot().sendMessage(message);
    }
}
