package org.wiese2.twootshome;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.wiese2.helper.Console;
import org.wiese2.helper.Storage;
import org.wiese2.twootshome.data.Home;
import org.wiese2.twootshome.homes.AdminHome;
import org.wiese2.twootshome.homes.PlayerHome;
import org.wiese2.twootshome.data.HomeManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class Main extends JavaPlugin {
    public static JavaPlugin instance = null;
    public HomeManager homeManager = null;

    @Override
    public void onEnable() {
        instance = this;

        this.homeManager = new HomeManager();
        this.homeManager.loadHomes();

        for (Map.Entry<String, Home> entry : homeManager.Homes.entrySet()) {
            entry.getValue().prepareLocation();
        }
    }

    @Override
    public void onDisable() {
        this.homeManager.saveHomes();
    }

    @Override
    public ArrayList<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            String user = player.getUniqueId().toString();

            if (isCommand(command, "homes")) {
                return list();
            } else if (isCommand(command, "sethome")) {
                if (args.length == 1) {
                    return list("home-name");
                }
                return list();
            } else if (isCommand(command, "delhome", "home")) {
                if (args.length == 1) {
                    return homeManager.listHomes(user);
                } else if (args.length == 2 && isCommand(command, "delhome")) {
                    return list("confirm");
                }
                return list();
            } else if (isCommand(command, "ahomes", "ahome")) {
                if (args.length == 2) {
                    if (isCommand(command, "ahomes")) {
                        return list();
                    }

                    Player target;
                    if ((target = Bukkit.getPlayerExact(args[0])) == null) {
                        return list();
                    }
                    user = target.getUniqueId().toString();

                    return homeManager.listHomes(user);
                } else if (args.length > 2) {
                    return list();
                }
            }
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (PlayerHome.doHomeCommands(this.homeManager, sender, player, command, label, args)) {
                return true;
            } else return AdminHome.doHomeCommands(this.homeManager, sender, player, command, label, args);
        }
        return false;
    }

    public static void doTeleportSound(Player player) {
        Location loc = player.getLocation();
        player.playSound(loc, Sound.ENTITY_ENDERMAN_TELEPORT, 0.3f, 1);
    }

    public boolean isCommand(Command command, String... compare) {
        for (String comp : compare) {
            if (command.getName().equalsIgnoreCase(comp)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<String> list(String... args) {
        return new ArrayList<>(Arrays.asList(args));
    }
}
