package org.wiese2.twootshome.data;

import org.wiese2.helper.Storage;
import org.wiese2.twootshome.Main;

import java.util.*;

public class HomeManager {
    public static final String EXTENSION = ".home";

    public HashMap<String, Home> Homes = new HashMap<>();
    private final Storage<Home> storageManager;

    public HomeManager() {
        storageManager = new Storage<>(Main.instance, EXTENSION, Home::new);
    }

    public void setHome(Home home) {
        String hash = home.getHash();

        this.Homes.put(hash, home);
    }

    public void removeHome(Home home) {
        String hash = home.getHash();

        this.Homes.remove(hash);

        storageManager.remove(home);
    }

    public Home getHome(Home home) {
        return this.Homes.get(home.getHash());
    }

    public String[] getHomes(String user) {
        return listHomes(user).toArray(new String[]{});
    }

    public ArrayList<String> listHomes(String user) {
        ArrayList<String> homes = new ArrayList<>();

        for (Map.Entry<String, Home> entry : this.Homes.entrySet()) {
            Home home = entry.getValue();

            if (home.getUser().equals(user)) {
                homes.add(home.getName());
            }
        }
        return homes;
    }

    public void loadHomes() {
        Main.instance.getLogger().info("Loading homes");

        this.Homes = storageManager.loadMapFromFiles();

        Main.instance.getLogger().warning("Successfully loaded " + this.Homes.size() + " homes");
    }

    public void saveHomes() {
        Main.instance.getLogger().info("Saving homes");

        if (storageManager.saveMapToFiles(this.Homes)) {
            Main.instance.getLogger().warning("Successfully saved " + this.Homes.size() + " homes");
        } else {
            Main.instance.getLogger().warning("Failed to save some homes");
        }
    }
}
