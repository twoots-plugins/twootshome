package org.wiese2.twootshome.data;

import org.bukkit.Location;
import org.bukkit.World;
import org.wiese2.helper.IStorageSerializable;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

public class Home extends IStorageSerializable {
    private MultiWorldLocation location;

    private String name;
    private String user;
    private String worldName;
    private Long worldSeed;

    public Home(Location location, String name, String user) {
        this.location = new MultiWorldLocation(location);
        this.name = name;
        this.user = user;

        World world = location.getWorld();
        if (world != null) {
            this.worldName = world.getName();
            this.worldSeed = world.getSeed();
        }
    }

    public Home() {

    }

    public String getHash() {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update((this.getName() + " " + this.getUser()).getBytes());
            byte[] digest = md.digest();
            return DatatypeConverter
                    .printHexBinary(digest).toUpperCase();
        } catch (NoSuchAlgorithmException ignore) {
            return "invalid-hash";
        }
    }

    public Map<String, Object> serialize() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("location", this.location.serialize());
        map.put("name", this.name);
        map.put("user", this.user);
        map.put("worldName", this.worldName);
        map.put("worldSeed", this.worldSeed);

        return map;
    }

    public Home deserialize(Map<String, Object> args) {
        Home home = new Home();

        home.setName((String) args.get("name"));
        home.setUser((String) args.get("user"));
        home.worldName = (String) args.get("worldName");
        home.worldSeed = (Long) args.get("worldSeed");

        home.location = (MultiWorldLocation) (new MultiWorldLocation()).deserialize((Map<String, Object>) args.get("location"));
        return home;
    }

    public boolean prepareLocation() {
        return this.location.prepareWorld(this.name, this.worldName, this.worldSeed);
    }

    public Location getLocation() {
        return location.getLocation();
    }

    public void setLocation(Location location) {
        this.location = new MultiWorldLocation(location);
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
